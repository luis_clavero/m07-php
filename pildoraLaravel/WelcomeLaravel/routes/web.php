<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('user');
});

Route::post('saludo', function () {
    $user = $_POST['username'];
    return view('welcomeUser')->with('user', $user);
});

Route::get('prueba', function () {
    return 'Prueba';
});