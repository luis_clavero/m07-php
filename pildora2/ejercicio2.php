<html>
  <head>
    <title>Ejercicio pildora 2</title>
  </head>
  <body>
    <?php /* 2. Haz una pagina web con un formulario y dos campos de
    texto y un desplegable que de a elegir entre los 5 operaciones 
    aritmeticas básicas. El objetivo es que al introducir dos numeros
    y seleccionar la operación se cargue una nueva pagina con el
    resultado de dicha operación */
    
    $firstNum = $_POST ["firstNum"];
    $secondNum = $_POST ["secondNum"];
    $option = $_POST ["option"];
    echo "La operación es " . $option;
    $result = 0;
    switch ($option) {
      case "sumar":
        $result = $firstNum + $secondNum;
        break;
      case "restar":
        $result = $firstNum - $secondNum;
        break;
      case "multiplicar":
        $result = $firstNum * $secondNum;
        break;
      case "dividir":
        $result = $firstNum / $secondNum;
        break;
      case "potenciacion":
        $result = $firstNum ** $secondNum;
        break;
    }
    echo "<br>El resultado de la operación es " . $result;
    ?> 
  </body>
</html>