var mongoose = require("mongoose"),
Alumno = require("../models/alumnos");

exports.getAllAlumnos = async () => {
    try {
        const alumnos = await Alumno.find({});
        return alumnos;
    } catch (error) {
        console.error(`Error getting all "Alumnos" ${error}`);
    }
};

exports.getAlumnoById = async (req) => {
    try {
        var id = req.params.id;
        const alumno = await Alumno.findById(id);
        return alumno;
    } catch (error) {
        console.error(`Error getting "Alumno" ${error}`);
    }
};

exports.deleteAlumnoById = async (req) => {
    try {
        var id = req.params.id;
        const alumno = await Alumno.findByIdAndDelete(id);
        return alumno;
    } catch (error) {
        console.error(`Error deleting "Alumno" from the database ${error}`);
    }
};

exports.updateAlumnoById = async (req) => {
    try {
        var id = req.params.id;
        var update = {
            nombre: req.body.nombre,
            apellido: req.body.apellido
        }
        var response = await Alumno.updateOne({_id: id}, update);
        return response;
    } catch (error) {
        console.error(`Error updating "Alumno" from the database ${error}`);
    }
};

exports.addAlumno = async (req) => {
    var alumno = {
        nombre: req.body.nombre,
        apellido: req.body.apellido
    };
    try {
        var newAlumno = new Alumno(alumno);
        var response = await newAlumno.save();
        return response;
    } catch (error) {
        console.error(`Error adding "Alumno" to the database ${error}`);
    }
};
