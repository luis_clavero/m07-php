var mongoose = require("mongoose"),
Asignatura = require("../models/asignatura");

exports.getAllAsignaturas = async () => {
    try {
        const asignaturas = await Asignatura.find({});
        return asignaturas;
    } catch (error) {
        console.error(`Error getting all "Asignaturas" ${error}`);
    }
};

exports.getAsignaturaById = async (req) => {
    try {
        var id = req.params.id;
        const asignatura = await Asignatura.findById(id);
        return asignatura;
    } catch (error) {
        console.error(`Error getting "Asignatura" ${error}`);
    }
};

exports.deleteAsignaturaById = async (req) => {
    try {
        var id = req.params.id;
        const asignatura = await Asignatura.findByIdAndDelete(id);
        return asignatura;
    } catch (error) {
        console.error(`Error deleting "Asignatura" from the database ${error}`);
    }
};

exports.updateAsignaturaById = async (req) => {
    try {
        var id = req.params.id;
        var update = {
            nombre: req.body.nombre,
            numHoras: req.body.numHoras,
            docente: "/api/docente/" + req.body.docente,
            alumnos: []
        }
        for (var i = 0; i < req.body.alumnos.length; i++) {
            update.alumnos.push(req.body.alumnos[i]);
        }
        var response = await Asignatura.updateOne({_id: id}, update);
        return response;
    } catch (error) {
        console.error(`Error updating "Asignatura" from the database ${error}`);
    }
};

exports.addAsignatura = async (req) => {
    var asignatura = {
        nombre: req.body.nombre,
        numHoras: req.body.numHoras,
        docente: "/api/docente/" + req.body.docente,
        alumnos: []
    }
    try {
        for (var i = 0; i < req.body.alumnos.length; i++) {
            asignatura.alumnos.push("/api/alumnos/" + req.body.alumnos[i]);
        }
        var newAsignatura = new Asignatura(asignatura);
        var response = await newAsignatura.save();
        return response;
    } catch (error) {
        console.error(`Error adding "Asignatura" to the database ${error}`);
    }
};
