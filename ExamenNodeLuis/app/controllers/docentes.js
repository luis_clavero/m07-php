var mongoose = require("mongoose"),
Docente = require("../models/docente");

exports.getAllDocentes = async () => {
    try {
        const docentes = await Docente.find({});
        return docentes;
    } catch (error) {
        console.error(`Error getting all "Docentes" ${error}`);
    }
};

exports.getDocenteById = async (req) => {
    try {
        var id = req.params.id;
        const docente = await Docente.findById(id);
        return docente;
    } catch (error) {
        console.error(`Error getting "Docente" ${error}`);
    }
};

exports.deleteDocenteById = async (req) => {
    try {
        var id = req.params.id;
        const docente = await Docente.findByIdAndDelete(id);
        return docente;
    } catch (error) {
        console.error(`Error deleting "Docente" from the database ${error}`);
    }
};

exports.updateDocenteById = async (req) => {
    try {
        var id = req.params.id;
        var update = {
            nombre: req.body.nombre,
            apellido: req.body.apellido
        }
        var response = await Docente.updateOne({_id: id}, update);
        return response;
    } catch (error) {
        console.error(`Error updating "Docente" from the database ${error}`);
    }
};

exports.addDocente = async (req) => {
    var docente = {
        nombre: req.body.nombre,
        apellido: req.body.apellido
    };
    try {
        var newDocente = new Docente(docente);
        var response = await newDocente.save();
        return response;
    } catch (error) {
        console.error(`Error adding "Docente" to the database ${error}`);
    }
};
