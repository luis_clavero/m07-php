var path = require("path");
var controllerDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();

// Controllers
var alumnosController = require(path.join(controllerDir, "alumnos"));
var asignaturasController = require(path.join(controllerDir, "asignaturas"));
var docentesController = require(path.join(controllerDir, "docentes"));


// Alumnos
router.get("/alumnos", async (req, res, next) => {
    var response = await alumnosController.getAllAlumnos();
    console.log(response);
    res.jsonp(response);
});

router.get("/alumnos/:id", async (req, res, next) => {
    var response = await alumnosController.getAlumnoById(req);
    console.log(response);
    res.jsonp(response);
});

router.delete("/alumnos/:id", async (req, res, next) => {
    var response = await alumnosController.deleteAlumnoById(req);
    console.log(response);
    res.jsonp(response);
});

router.put("/alumnos", async (req, res, next) => {
    var response = await alumnosController.updateAlumnoById(req);
    console.log(response);
    res.jsonp(response);
});

router.post("/alumnos/new", async (req, res, next) => {
    var response = await alumnosController.addAlumno(req);
    console.log(response);
    res.jsonp(response);
});


// Docentes
router.get("/docentes", async (req, res, next) => {
    var response = await docentesController.getAllDocentes();
    console.log(response);
    res.jsonp(response);
});

router.get("/docentes/:id", async (req, res, next) => {
    var response = await docentesController.getDocenteById(req);
    console.log(response);
    res.jsonp(response);
});

router.delete("/docentes/:id", async (req, res, next) => {
    var response = await docentesController.deleteDocenteById(req);
    console.log(response);
    res.jsonp(response);
});

router.put("/docentes", async (req, res, next) => {
    var response = await docentesController.updateDocenteById(req);
    console.log(response);
    res.jsonp(response);
});

router.post("/docentes/new", async (req, res, next) => {
    var response = await docentesController.addDocente(req);
    console.log(response);
    res.jsonp(response);
});


// Asignaturas
router.get("/asignaturas", async (req, res, next) => {
    var response = await asignaturasController.getAllAsignaturas();
    console.log(response);
    res.jsonp(response);
});

router.get("/asignaturas/:id", async (req, res, next) => {
    var response = await asignaturasController.getAsignaturaById(req);
    console.log(response);
    res.jsonp(response);
});

router.delete("/asignaturas/:id", async (req, res, next) => {
    var response = await asignaturasController.deleteAsignaturaById(req);
    console.log(response);
    res.jsonp(response);
});

router.put("/asignaturas", async (req, res, next) => {
    var response = await asignaturasController.updateAsignaturaById(req);
    console.log(response);
    res.jsonp(response);
});

router.post("/asignaturas/new", async (req, res, next) => {
    var response = await asignaturasController.addAsignatura(req);
    console.log(response);
    res.jsonp(response);
});

module.exports = router;