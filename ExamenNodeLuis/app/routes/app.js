var express = require("express");
var path = require("path");
var router = express.Router();
var controllerDir = "/app/app/controllers";
var alumnosController = require(path.join(controllerDir, "alumnos"));
var asignaturasController = require(path.join(controllerDir, "asignaturas"));
var docentesController = require(path.join(controllerDir, "docentes"));

router.get("/chat/:id", async (req, res, next) => {
    res.render("chat.pug");
});

router.get("/asignatura/new", async (req, res, next) => {
    var alumnos = await alumnosController.getAllAlumnos();
    var docentes = await docentesController.getAllDocentes();
    res.render("newAsignatura.pug", {alumnos: alumnos, docentes: docentes});
});

router.post("/asignatura/save", async (req, res, next) => {
    var newAsignatura = await asignaturasController.addAsignatura(req);
    res.jsonp(newAsignatura);
});

module.exports = router;