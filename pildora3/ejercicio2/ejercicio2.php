<html>
  <head>
    <title>Ejercicio 2</title>
  </head>
  <body>
    <?php /* 2.Dado el siguiente array $loteria = array(61, 32, 43, 61). 
    Haz un programa que muestre por pantalla el numero de veces que 
    aparece el numero 61 */
    $loteria = [61, 32, 43,61];
    $searchedNum = 61;
    $counter = 0;
    for ($i = 0; $i < count($loteria); $i++) {
      // Check if the number in the i position is $searchedNum
      if ($loteria[$i] == $searchedNum) {
        $counter++;
      }
    }
    echo "El numero " . $searchedNum . " aparece " . $counter . " veces en 
    la lista";
    ?> 
  </body>
</html>