<html>
  <head>
    <title>Ejercicio 3</title>
  </head>
  <body>
    <?php /* 3. Utilizando el array del ejercicio anterior, crea un programa que cuente 
    el numero de veces que aparece un numero dado a traves de un formulario post. 
    En la pagina del resultado ha de aparecer el numero que se busca y el numero de 
    coincidencias en una frase 'bonita' */
    $loteria = [61, 32, 43, 61];
    $searchedNum = $_POST ["searchedNum"];
    $counter = 0;
    for ($i = 0; $i < count($loteria); $i++) {
      // Check if the number in the i position is $searchedNum
      if ($loteria[$i] == $searchedNum) {
        $counter++;
      }
    }
    ?>
    <div style = "background-color: black"><p style = "color: green; font-family: 'Courier New', Courier, monospace;">El numero a buscar es <?php echo $searchedNum?> y ha aparecido
    <?php echo $counter?> veces</p></div> 
  </body>
</html>