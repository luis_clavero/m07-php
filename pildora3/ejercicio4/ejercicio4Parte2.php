<html>
  <head>
    <title>Ejercicio 4 parte 2</title>
  </head>
  <body>
    <?php
      // Get the number of matches and players
      $numberOfPlayers = count($_POST ["players"]);
      $numberOfMatches = count($_POST ["matches"]) / $numberOfPlayers;
      $arrayPlayersMatches = array();
      // This var is used to access the position of the matches array
      $incrementalCounter = 0;
      // Insert all the values to the array
      for ($i = 0; $i < $numberOfPlayers; $i++) {
        $arrayPlayersMatches[$i] = array($_POST["players"][i]);
        for ($j = 0; $j < $numberOfMatches; $j++) {
          $arrayPlayersMatches[$i] = $_POST 
          ["matches"][$j + $incrementalCounter]);
        }
        /* Increment the counter with the number of matches so we can access to
        the values of the next player */
        $incrementalCounter += $numberOfMatches;
      }
      var_dump($arrayPlayersMatches);
    ?>
  </body>
</html>