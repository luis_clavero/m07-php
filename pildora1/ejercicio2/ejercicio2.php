<html>
  <head>
    <title>Ejercicio 2</title>
  </head>
  <body>
    <?php /* 2. Crea una pagina en la que se almacenan dos numeros 
    en variables y muestre en una tabla los valores que toman las
    variables y cada uno de los resultados de todas las operaciones 
    aritmeticas. */ 
    $num1 = 4;
    $num2 = 3;
    echo '<table>
      <tr>
        <th>Operacion</th>
        <th>Valor</th>
      </tr>
      <tr>
        <td>A</td>
        <td>'.$num1.'</td>
      </tr>
      <tr>
        <td>B</td>
        <td>'.$num2.'</td>
      </tr>
      <tr>
        <td>A+B</td>
        <td>'.($num1 + $num2).'</td>
      </tr>
      <tr>
        <td>A-B</td>
        <td>'.($num1 - $num2).'</td>
      </tr>
      <tr>
        <td>A*B</td>
        <td>'.($num1 * $num2).'</td>
      </tr>
      <tr>
        <td>A/B</td>
        <td>'.($num1 / $num2).'</td>
      </tr>
      <tr>
        <td>AexpB</td>
        <td>'.pow($num1,$num2).'</td>
      </tr>
    </table>';
    ?> 
  </body>
</html>