<?php
namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{    
   
    private $products;
    private $_filters;

    public function __construct()
    {


        $this->products = new Product();

        /**
         * Filters (name=>value) format to show in the view
         * Write the content of the stars
         */
        $this->_filters=(object)array(
            'category'=>array($this->products->select('categoria')->groupBy('categoria')->get()),
            'stars'=>array($this->products->select('puntuacion')->groupBy('puntuacion')->orderBy('puntuacion', 'asc')->get())
        );

    }
    /**
     * Method to list all the products
     */
    public function all()
    {  
        $showBanner = true;
        return view('examenViews/products')->with('products', $this->products->get())->with('filters', $this->_filters)->with('showBanner', $showBanner);
    }

    /**
     * Method to list the products filtered by category
     */
    public function category($category)
    {   
        $showBanner = false;
        $filters = Product::query();

        
        $filters->category($category);

        $productsTmp = $filters->get();

        return view('examenViews/products')->with('products', $productsTmp)->with('showBanner', $showBanner)->with('filters', $this->_filters);
    }

    /**
     * Method to list the products filtered by stars
     */
    public function stars(Request $request)
    {
        $showBanner = false;
        $filters = Product::query();
        $stars = $request->stars;

        if(!empty($stars)) {
            $filters->stars($stars);
        }

        $productsTmp = $filters->get();

        return view('examenViews/products')->with('products', $productsTmp)->with('showBanner', $showBanner)->with('filters', $this->_filters);
    }
}