<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function scopeStars($query, $starsArray) {
        return $query->whereIn('puntuacion', $starsArray);
    }

    public function scopeCategory($query, $category) {
        return $query->where('category', '=', $category);
    }

    public function scopeOrderByPrice($query) {
        return $query->orderBy('price','desc');
    }
}
