<!--Use this file as layout of your hole APP-->

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@yield('head')

<body>
@yield('navbar')
@yield('shopContent')
@yield('footer')
</body>
</html>