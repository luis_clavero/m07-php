<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'nombre'=>'Lego Creator',
            'price'=> 24.95,
            'descripcion'=>'Lego Creator 3 en 1',
            'puntuacion'=>4,
            'categoria'=>'Tiendas',
            'imagen' => base64_encode(file_get_contents("public/img/lego1.jpeg"))
        ]);
        DB::table('products')->insert([
            'nombre'=>'Lego City',
            'price'=>19.95,
            'descripcion'=>'Lego city gasolinera con coches',
            'puntuacion'=>2,
            'categoria'=>'Automobiles',
            'imagen' => base64_encode(file_get_contents("public/img/lego2.jpeg"))
        ]);
        DB::table('products')->insert([
            'nombre'=>'Lego Friends',
            'price'=> 40,
            'descripcion'=>'Lego casa Friends',
            'puntuacion'=> 5,
            'categoria'=>'Series animacion',
            'imagen' => base64_encode(file_get_contents("public/img/lego3.jpeg"))
        ]);
        DB::table('products')->insert([
            'nombre'=>'Lego Classic',
            'price'=> 34.95,
            'descripcion'=>'Lego Classic 1500 piezas',
            'puntuacion'=> 5,
            'categoria'=>'Clasicos',
            'imagen' => base64_encode(file_get_contents("public/img/lego4.jpeg"))
        ]);
    }
}
