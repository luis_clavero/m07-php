<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ProductController::class, 'all']);
Route::post('/filterByStars', [ProductController::class, 'stars']);
Route::get('/filterByCategory/{category}', [ProductController::class, 'category']);

Route::get('/register', function () {
    return view('register');
})->name('register');;

Route::get('/login', function () {
    return view('login');
})->name('login');;
//Auth::routes();