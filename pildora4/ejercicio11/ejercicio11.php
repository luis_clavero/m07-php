<html>
  <head>
    <meta charset="utf-8">
    <title>Ejercicio 11 php pildora 4</title>
  </head>
  <body>
      <?php
        class WebpageHeader {
            private $title;
            private $position;
            private $fontColor;
            private $backgroundColor;
            
            /**
             * Constructor for the "WebpageHeader" object
             * 
             * @param string $title the title of the header
             * @param string $position the position of the header
             * @param string $fontColor the color of the font
             * @param string $backgroundColor the bg color
             * @return void
             */
            public function __construct($title, $position, $fontColor, $backgroundColor) {
                $this -> title = $title;
                $this -> position = $position;
                $this -> fontColor = $fontColor;
                $this -> backgroundColor = $backgroundColor;
            }

            /**
            * Show Webpage header via html
            *
            * @param void
            * @return void
            */
            public function show() {
                echo '<div style="font-size:40px;color:' . $this -> fontColor . ';background-color:' 
                . $this -> backgroundColor . ';text-align:' . $this -> position . '">';
                echo $this -> title . '</div>';
            }
        }
        $title = $_POST["title"];
        $position = $_POST["position"];
        $fontColor = $_POST["fontColor"];
        $bgColor = $_POST["bgColor"];
        $header = new WebpageHeader($title, $position, $fontColor, $bgColor);
        $header -> show();
      ?>
  </body>
</html>