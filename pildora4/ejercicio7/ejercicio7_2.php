<html>
  <head>
    <meta charset="utf-8">
    <title>Ejercicio 7_2 php pildora 4</title>
  </head>
  <body>
    <form method = "post" action = "ejercicio7_2.php">
    <?php
    /* 7. Realiza una función separar(lista) que tome una lista de números enteros 
    y devuelva (no imprimir) dos listas ordenadas. La primera con los números pares 
    y la segunda con los números impares. */
    function separateNumbers($numberList) {
        $evenNumbers = [];
        $oddNumbers = [];
        for ($i = 0; $i < count($numberList); $i++) {
            if ($numberList[$i] % 2 == 0) {
                $evenNumbers[] = $numberList[$i];
            } else {
                $oddNumbers[] = $numberList[$i];
            }
        }
        return array($evenNumbers, $oddNumbers);
    }
    $tmp = separateNumbers($_POST['listOfNumbers']);
    echo "<p>Pares: </p>";
    var_dump($tmp[0]);
    echo "<br>";
    echo "<p>Impares: </p>";
    var_dump($tmp[1]);
    ?>
  </body>
</html>