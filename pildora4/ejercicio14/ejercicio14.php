<html>
  <head>
    <meta charset="utf-8">
    <title>Ejercicio 11 php pildora 4</title>
  </head>
  <body>
      <?php
        class WebpageHeader {
            private $title;
            private $position;
            private $fontColor;
            private $backgroundColor;
            
            /**
             * Constructor for the "WebpageHeader" object
             * 
             * @param void no parameters
             * @return void
             */
            public function __construct() {
                $this -> title = "Default title";
                $this -> position = "center";
                $this -> fontColor = "white";
                $this -> backgroundColor = "blue";
            }

            /**
            * Show Webpage header via html
            *
            * @param void no parameters
            * @return void
            */
            public function show() {
                echo '<div style="font-size:40px;color:' . $this -> fontColor . ';background-color:' 
                . $this -> backgroundColor . ';text-align:' . $this -> position . '">';
                echo $this -> title . '</div>';
            }
        }
        $header = new WebpageHeader();
        $header -> show();
      ?>
  </body>
</html>