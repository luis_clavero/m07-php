<html>
  <head>
    <title>Ejercicio 6 pildora 4</title>
  </head>
  <body>
    <?php 
    /* 6. Realiza una función llamada intermedio(a, b) que a partir de dos números,
    devuelva su punto intermedio. Cuando lo tengas comprueba el punto intermedio 
    entre -12 y 24 */
    $nums = $_POST['nums'];
    function middlePoint ($num1, $num2) {
        $average = ($num1 + $num2) / 2;
        return $average;
    }
    ?>
    <p><?php 
      $tmp = middlePoint($nums[0], $nums[1]);
      echo "Punto intermedio: " . $tmp;
      ?>
    </p>
  </body>
</html>