<html>
  <head>
    <meta charset="utf-8">
    <title>Ejercicio 09 pildora 4</title>
  </head>
  <body>
      <?php
        class ListOfLinks {
            private $links;
            
            /**
             * Initialize the "ListOfLinks" object
             * 
             * @param string[] $links the links of the list
             * @return void
             */
            public function initialize($links) {
                $this -> links = $links;
            }
            
            /**
             * Add a link to the list
             * 
             * @param string $link the link to be added
             * @return void
             */
            public function uploadLink($link) {
                array_push($this -> links, $link);
            }

            /**
            * Show the list of links via html
            *
            * @param void
            * @return void
            */
            public function show() {
                for ($i = 0; $i < count($this -> links); $i++) {
                    echo '<a href="' . $this -> links[$i] . '">Link ' . $i . '</a><br>';    
                }
            }
        }

        $someLinks = [];
        $links = new ListOfLinks();
        $links -> initialize($someLinks);
        $links -> uploadLink("https://www.google.es");
        $links -> uploadLink("https://www.youtube.es");
        $links -> uploadLink("https://www.twitter.es");
        $links -> show();
      ?>
  </body>
</html>