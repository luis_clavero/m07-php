<html>
  <head>
    <title>Ejercicio 3 pildora 4</title>
  </head>
  <body>
    <?php 
    /* 4. Crear un formulario en el que se introduce el precio de una compra y realizar el programa que calcula mediante una función la siguientes reglas. */
    $price = $_POST['price'];
    function discountPrice ($totalPrice) {
      if ($totalPrice < 100) {
        return $totalPrice;
      } else if ($totalPrice >= 100 && $totalPrice <= 499.99) {
        return $totalPrice * 0.9;
      } else {
        return $totalPrice * 0.85;
      }
    }
    ?>
    <p><?php 
      $tmp = discountPrice($price);
      echo "Precio final: " . $tmp;
      ?>
    </p>
  </body>
</html>