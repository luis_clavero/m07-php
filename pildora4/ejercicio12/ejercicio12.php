<?php
class Tabla {
  private $mat=array();
  private $cantFilas;
  private $cantColumnas;
  public function __construct($fi,$co)
  {
    $this -> cantFilas = $fi;
    $this -> cantColumnas = $co;
    for ($i = 0; $i < $fi; $i++) {
      $this -> mat[$i] = [];
    }
  }
 
  public function cargar($fila,$columna,$valor)
  {
    $this->mat[$fila][$columna]=$valor;
  }
 
  private function inicioTabla()
  {
    echo '<table border="1">';
  }
 
  private function inicioFila()
  {
    echo '<tr>';
  }
 
  private function mostrar($fi,$co)
  {
    echo '<td>'.$this->mat[$fi][$co].'</td>';
  }
 
  private function finFila()
  {
    echo '</tr>';
  }
 
  private function finTabla()
  {
    echo '</table>';
  }
 
  public function graficar()
  {
    // Create the table
    $this -> inicioTabla();
    for ($i = 0; $i <= $this -> cantFilas; $i++) {
      // Create a row
      $this -> inicioFila();
      for ($j = 0; $j <= $this -> cantColumnas; $j++) {
        // Add the value of the square to the table
        $this -> mostrar($i, $j);
      }
      $this -> finFila();
    }
    // Close the table
    $this -> finTabla();
  }
}
 
$tabla1=new Tabla(2,3);
$tabla1->cargar(1,1,"1");
$tabla1->cargar(1,2,"2");
$tabla1->cargar(1,3,"3");
$tabla1->cargar(2,1,"4");
$tabla1->cargar(2,2,"5");
$tabla1->cargar(2,3,"6");
$tabla1->graficar();
?>