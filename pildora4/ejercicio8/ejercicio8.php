<html>
  <head>
    <meta charset="utf-8">
    <title>Ejercicio 8 html pildora 4</title>
  </head>
  <body>
      <?php
        class Empleado {
            private $name;
            private $monthlyWage;
            
            
            /**
             * Initialize the "Empleado" object
             * 
             * @param string $name the worker's name
             * @param integer $monthlyWage the worker's monthly wage
             * @return void
             */
            public function initialize($name, $monthlyWage) {
                $this -> name = $name;
                $this -> monthlyWage = $monthlyWage; 
            }
            
            /**
             * Print the worker's information dependinga on it's wage
             * 
             * @param void
             * @return string 
             */
            public function printInfo() {
                echo $this -> name;
                echo "<br>";
                if ($this -> monthlyWage > 3000) {
                    echo "Sueldo de " . $this -> monthlyWage . "€, paga impuestos";
                } else {
                    echo "Sueldo de " . $this -> monthlyWage . "€, no paga impuestos";
                    
                }
                echo "<br><br>";
            }
        }

        $worker1 = new Empleado();
        $worker1 -> initialize("Luis", 2000);
        $worker1 -> printInfo();

        $worker2 = new Empleado();
        $worker2 -> initialize("Mario", 3500);
        $worker2 -> printInfo();
      ?>
  </body>
</html>