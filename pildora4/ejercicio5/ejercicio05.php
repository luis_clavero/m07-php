<html>
  <head>
    <title>Ejercicio 5 pildora 4</title>
  </head>
  <body>
    <?php 
    /* 5. Realiza una función llamada relacion(a, b) que a partir de 
    dos números cumpla lo siguiente:  
    Si el primer número es mayor que el segundo, debe devolver 1.
    Si el primer número es menor que el segundo, debe devolver -1.
    Si ambos números son iguales, debe devolver un 0.*/
    $nums = $_POST['nums'];
    function checkGreatest ($num1, $num2) {
      if ($num1 > $num2) {
        return 1;
      } else if ($num1 < $num2) {
          return -1;
      } else {
          return 0;
      }
    }
    ?>
    <p><?php 
      $tmp = checkGreatest($nums[0], $nums[1]);
      echo "Resultado: " . $tmp;
      ?>
    </p>
  </body>
</html>